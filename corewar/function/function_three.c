/*
** function_three.c for  in /home/teyssa_r/rendu/CPE_2014_corewar/corewar/function
** 
** Made by teyssa_r teyssa_r
** Login   <teyssa_r@epitech.net>
** 
** Started on  Wed Aug 20 15:16:05 2014 teyssa_r teyssa_r
** Last update Sun Aug 31 22:10:43 2014 teyssa_r teyssa_r
*/

/*
** brief: la description
** @param1: a quoi il sert
** @param2: a quoi il sert
** @param3: a quoi il sert
** @param4: a quoi il sert
** return: ce que la fonction retourne
*/
void	_zjmp(t_list *champ, char *mem, int *arg)
{
  if (champ->carry == 1)
    champ->pc = champ->pc + (arg[0] % IDX_MOD) % MEM_SIZE;
}

/*
** brief: la description
** @param1: a quoi il sert
** @param2: a quoi il sert
** @param3: a quoi il sert
** @param4: a quoi il sert
** return: ce que la fonction retourne
*/
void	_ldi(t_list *champ, char *mem, int *arg)
{
  
}

/*
** brief: la description
** @param1: a quoi il sert
** @param2: a quoi il sert
** @param3: a quoi il sert
** @param4: a quoi il sert
** return: ce que la fonction retourne
*/
void	_sti(t_list *champ, char *mem, int *arg)
{
  int	i;

  i = 0;
  if (type_arg(arg[0], 10, 6, 0) / 100 == 1)
    while (i < REG_SIZE)
      {
	mem[arg[2] + arg[3] + i] = arg[1] >> i;
	i++;
      }
}

/*
** brief: la description
** @param1: a quoi il sert j
** @param2: a quoi il sert
** @param3: a quoi il sert
** @param4: a quoi il sert
** return: ce que la fonction retourne
*/
void		_fork(t_list *champ, char *mem, int *arg)
{
  i = 0;
  while (i < champ->chpm->prog_size)
    {
      mem[(champ->pc + arg[0] % IDX_MOD + i) % MEM_SIZE] =
	mem[(champ->init_pos + i) % MEM_SIZE];
      i++;
    }
}
