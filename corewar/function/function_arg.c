/*
** function_arg.c for  in /home/viterb_c/Corewar/corewar/corewar/function
**
** Made by charles viterbo
** Login   <viterb_c@epitech.net>
**
** Started on  Sun Aug 31 19:58:11 2014 charles viterbo
** Last update Sun Aug 31 22:09:45 2014 charles viterbo
*/

#include "struct.h"
#include "op.h"

/*
** brief: osef
** @octet: osef
** @func: code instru
** @i: put 6 -> norme
** @j: put 0 -> norme
*/
int	type_arg(unsigned char octet, int func, int i, int j)
{
  int	rslt;
  char	tmp;

  rslt = 0;
  while (i > 0)
    {
      rslt *= 10;
      if ((tmp = ((octet >> i) & 0x03)) != 0)
	{
	  tmp & op_tab[func].type[j];
	  if (tmp == 1)
	    rslt += T_REG;
	  else if (tmp == 2)
	    rslt += T_DIR;
	  else if (tmp == 3)
	    rslt += T_IND;
	  else
	    return (-1);
	}
      i = i - 2;
      j = j + 1;
    }
  return (rslt);
}

/*
** brief : this function will fill our tab *
** @champ : our champion
** @memory : our memory
** tab : our tab of int
*/
void	tab_param(t_list *champ, unsigned char *memory, int *tab)
{
  int	i;
  int	j;
  int	tmp;

  i = 1;
  j = 1;
  tmp = tab[0];
  while (tmp / i >= 10)
    i = i * 10;
  tmp = 1;
  while (i > 0)
    {
      if (((tab[0] / i) % 10 == T_REG) || ((tab[0] / i) % 10 == T_DIR))
	{
	  tab[j] = convert_bytes_int(&memory[champ->pc + tmp], REG_SIZE);
	  tmp = tmp + REG_SIZE;
	}
      else if ((tab[0] / i) % 10 == T_IND)
	{
	  tab[j] = convert_bytes_int(&memory[champ->pc + tmp], IND_SIZE);
	  tmp = tmp + IND_SIZE;
	}
      i = i / 10;
    }
}
