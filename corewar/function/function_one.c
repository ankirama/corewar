/*
** function_one.c for  in /home/teyssa_r/rendu/CPE_2014_corewar/corewar/function
** 
** Made by teyssa_r teyssa_r
** Login   <teyssa_r@epitech.net>
** 
** Started on  Wed Aug 20 15:00:31 2014 teyssa_r teyssa_r
<<<<<<< HEAD
** Last update Sun Aug 31 21:39:01 2014 charles viterbo
=======
** Last update Sun Aug 31 22:08:39 2014 teyssa_r teyssa_r
>>>>>>> 168efbed7ed1d1417e1b205386ad6e6d7d2f8a47
*/

#include "utils.h"

/*
** brief: do a live for nb_champ
** @root: list of champion
** @mem: our mem
** @arg: our arguments
** @nb_champ: number of champion
*/
void		_live(t_list *champ, char *mem, int *arg)
{
  t_list	*champion;

  champion = champ->next;
  while (champion != champ && champion->reg[0] != arg[0])
    champion = champion->next;
  if (champion->reg[0] == arg[1])
    {
      champion->live = 1;
      my_write(1, "le joueur ", 10);
      my_putnbr(champion->reg[0]);
      my_write(1, " (", 2);
      my_write(1, champion->chpm->prog_name, PROG_NAME_LENGTH + 1);
      my_write(1, ") est en vie.\n", 14);
    }
  champ->pc = champ->pc + 5;
}

/*
** brief: copy register in an other register
** @root: list of champion
** @mem: our mem
** @arg: our arguments
** @nb_champ: number of champion
*/
<<<<<<< HEAD
int	_st(t_list *root, char *mem, int *arg)
=======
void	_st(t_list *champ, char *mem, int *arg)
>>>>>>> 168efbed7ed1d1417e1b205386ad6e6d7d2f8a47
{
  int	nb;

  nb = type_arg(arg[0], 2, 6, 0);
  if (nb / 10 == 1 && nb % 10 == 1)
    arg[2] = arg[1];
  else if (nb / 10 == 1 && (nb % 10 == 2 || nb % 10 == 14))
    mem[champ->pc + (arg[2] % IDX_MOD)) % MEM_SIZE] = arg[1];
}

/*
** brief: loop to compose our new register
** @arg: our arguments
** @root: int to increment
** @mem: retenu
** @nb_champ: result
*/
int	_add2(int *arg, int i, int ret, int res)
{
  while (i < REG_SIZE * 8)
    {
      if (((arg[1] & 1) & (arg[2] & 1)) == 1)
        {
          if (ret == 1)
            res = res + (1 << i);
          ret = 1;
        }
      else if (((arg[1] & 1) | (arg[1] & 1)) == 1)
        {
          if (ret == 0)
            res = res + (1 << i);
        }
      else if (ret == 1)
        {
          res = res + (1 << i);
          ret = 0;
        }
      arg[1] = arg[1] >> 1;
      arg[2] = arg[2] >> 1;
      ++i;
    }
  return (res);
}

/*
** brief: add 2 register in 1 register
** @root: list of champion
** @mem: our mem
** @arg: our arguments
** @nb_champ: number of champion
*/
void		_add(t_list *root, char *mem, int *arg, int const nb_champ)
{
  t_list	*champ;
  int		i;
  int		ret;
  int		res;

  i = 0;
  res = 0;
  res = _add2(arg, i, ret, res);
  arg[3] = res;
  champ->carry = !champ->carry;
}
