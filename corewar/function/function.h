/*
** function.h for  in /home/teyssa_r/rendu/CPE_2014_corewar/corewar/function
** 
** Made by teyssa_r teyssa_r
** Login   <teyssa_r@epitech.net>
** 
** Started on  Wed Aug 20 15:02:40 2014 teyssa_r teyssa_r
** Last update Sun Aug 31 22:10:26 2014 teyssa_r teyssa_r
*/

#ifndef FUNCTION_H_
# define FUNCTION_H_

/*
** FUNCTION_ONE_C_
*/

void	_live(t_list *root, char *mem, int *arg);
void	_st(t_list *root, char *mem, int *arg);
void	_add(t_list *root, char *mem, int *arg);

/*
** FUNCTION_TWO_C_
*/

void	_sub(t_list *root, char *mem, int *arg);
int	_and(t_list *root, char *mem, int *arg);

/*
** FUNCTION_THREE_C_
*/

void	_zlmp(t_list *root, char *mem, int *arg);
void	_ldi(t_list *root, char *mem, int *arg);
void	_sti(t_list *root, char *mem, int *arg);
void	_fork(t_list *root, char *mem, int *arg);

/*
** FUNCTION_FOUR_C_
*/

void	_lldi(t_list *root, char *mem, int *arg);
void	_lfork(t_list *root, char *mem, int *arg);
void	_aff(t_list *root, char *mem, int *arg);

/*
** FUNCTION_XOR_OR_C_
*/

int	_or(t_list *root, char *mem, int *arg);
int	_xor(t_list *root, char *mem, int *arg);

/*
** LD_C_
*/

void	_ld(t_list *root, char *mem, int *arg);

/*
** LLD_C_
*/

void	_lld(t_list *root, char *mem, int *arg);

#endif /* !FUNCTION_H_ */
