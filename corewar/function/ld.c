/*
** ld.c for  in /home/viterb_c/rendu/CPE_2014_corewar/corewar/function
** 
** Made by charles viterbo
** Login   <viterb_c@epitech.net>
** 
** Started on  Wed Aug 20 17:12:14 2014 charles viterbo
** Last update Sun Aug 31 21:52:04 2014 teyssa_r teyssa_r
*/

#include "op.h"

/*
** brief: load in register
** @root: list of champion
** @mem: our mem
** @arg: our arguments
** @nb_champ: number of champion
*/
static void	_ld_register(t_list *champ, char *mem, int const *arg)
{
  int		i;

  i = -1;
  while (++i < REG_SIZE)
    champ->reg[argv[2]][i] = champ->reg[argv[1][i]];
}

/*
** brief: load in register
** @root: list of champion
** @mem: our mem
** @arg: our arguments
** @nb_champ: number of champion
*/
static void	_ld_direct(t_list *champ, char *mem, int const *arg)
{
  champ->reg[argv[2]] = arg[1];
}

/*
** brief: load in register
** @root: list of champion
** @mem: our mem
** @arg: our arguments
** @nb_champ: number of champion
*/
static void	_ld_indirect(t_list *champ, char *mem, int const *arg)
{
  int		i;

  i = -1;
  while (++i < REG_SIZE)
    champ->reg[argv[2]][i] = mem[champ->pc + (arg[1] % IDX_MOD) + i];
}

/*
** brief: load in register
** @root: list of champion
** @mem: our mem
** @arg: our arguments
** @nb_champ: number of champion
*/
int            ld(t_list *root, char *mem, int const *arg, int const nb_champ)
{
  int		nb;
  t_list	*champ;

  nb = type_arg(arg[0], 1, 6, 0);
  champ = root->next;
  while (champ->root != 0 && champ->root != nb_champ)
    champ = champ->next;
  if (champ->root == 0)
    return (-1);
  champ->carry = !champ->carry;
  if (nb / 10 == 1 && nb % 10 == 1)
    _ld_register(champ, mem, arg);
  else if (nb / 10 == 1 && nb % 10 == 2)
    _ld_direct(champ, mem, arg);
  else if (nb / 10 == 1 && nb % 10 == 4)
    _ld_indirect(champ, mem, arg);
  return (0);
}
