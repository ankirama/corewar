/*
** function_xor_or.c for  in /home/teyssa_r/rendu/CPE_2014_corewar/corewar/function
** 
** Made by teyssa_r teyssa_r
** Login   <teyssa_r@epitech.net>
** 
** Started on  Thu Aug 28 14:04:05 2014 teyssa_r teyssa_r
** Last update Sun Aug 31 22:06:03 2014 teyssa_r teyssa_r
*/

/*
** brief: function or '|'
** @root: our list
** @mem: our memoire
** @arg: our arguments
** @nb_champ: number of champion
*/
int		_or(t_list *root, char *mem, int *arg, int const nb_champ)
{
  int		i;
  t_list	*champ;

  champ->carry = !champ->carry;
  i = -1;
  if (nb % 100 == 1 && (nb % 10 == 1 || nb / 10 == 1))
    {
      if (arg[1] == T_REG)
        _and_two_reg(arg[1], arg[3], arg[2]);
      else if (arg[2] == T_REG)
        _and_two_reg(arg[2], arg[3], arg[1]);
    }
  else if (nb % 100 == 1 && nb % 10 != 1 && nb / 10 != 1)
    while (arg[3][++i])
      arg[3][i] = arg[1] | arg[2];
  else if (nb % 100 == 1 && nb % 10 == 1 && nb / 10 == 1)
    while (arg[1][++i] && arg[2][i] && arg[3][i])
      arg[3][i] = arg[1][i] | arg[2][i];
}

/*
** brief: loop to compose our new register
** @arg: our arguments
** @root: int to increment
** @mem: retenu
** @nb_champ: result
*/
static void	_or_two_reg(int *reg1, int *reg2, int val)
{
  int		i;

  i = -1;
  while (arg[1][++i] && arg[2][i] && arg[3][i])
    reg2[i] = val | reg1[i];
}

/*
** brief: function xor '^'
** @root: our list
** @mem: our memoire
** @arg: our arguments
** @nb_champ: number of champion
*/
int		_xor(t_list *root, char *mem, int *arg, int const nb_champ)
{
  int		i;
  t_list	*champ;

  champ->carry = !champ->carry;
  i = -1;
  if (nb % 100 == 1 && (nb % 10 == 1 || nb / 10 == 1))
    {
      if (arg[1] == T_REG)
        _and_two_reg(arg[1], arg[3], arg[2]);
      else if (arg[2] == T_REG)
        _and_two_reg(arg[2], arg[3], arg[1]);
    }
  else if (nb % 100 == 1 && nb % 10 != 1 && nb / 10 != 1)
    while (arg[3][++i])
      arg[3][i] = arg[1] ^ arg[2];
  else if (nb % 100 == 1 && nb % 10 == 1 && nb / 10 == 1)
    while (arg[1][++i] && arg[2][i] && arg[3][i])
      arg[3][i] = arg[1][i] ^ arg[2][i];
}

/*
** brief: loop to compose our new register
** @arg: our arguments
** @root: int to increment
** @mem: retenu
** @nb_champ: result
*/
static void	_and_two_reg(int *reg1, int *reg2, int val)
{
  int		i;

  i = -1;
  while (arg[1][++i] && arg[2][i] && arg[3][i])
    reg2[i] = val ^ reg1[i];
}
