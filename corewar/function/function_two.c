/*
** function_two.c for  in /home/teyssa_r/rendu/CPE_2014_corewar/corewar/function
** 
** Made by teyssa_r teyssa_r
** Login   <teyssa_r@epitech.net>
** 
** Started on  Wed Aug 20 15:15:21 2014 teyssa_r teyssa_r
** Last update Sun Aug 31 22:09:36 2014 teyssa_r teyssa_r
*/

/*
** brief: loop to compose our new register
** @arg: our arguments
** @root: int to increment
** @mem: retenu
** @nb_champ: result
*/
int     _sub2(int *arg, int i, int ret, int res)
{
  while (i < REG_SIZE * 8)
    {
      if (((arg[1] & 1) & (arg[2] & 1)) == 1 && ret == 1)
        {
          if (ret == 1)
            res = res + (1 << i);
          ret = 1;
        }
      else if (((arg[1] & 1) | (arg[1] & 1)) == 1)
        {
          if ((arg[1] & 1) == 0 && ret == 0)
            {
              res = res + (1 << i);
              ret = 1;
            }
          else if (ret == 0 && ((arg[1] & 1) == 1))
            res = res + (1 << i);
        }
      arg[1] = arg[1] >> 1;
      arg[2] = arg[2] >> 1;
      ++i;
    }
  return (res);
}

/*
** brief: la description
** @param1: a quoi il sert
** @param2: a quoi il sert
** @param3: a quoi il sert
** @param4: a quoi il sert
** return: ce que la fonction retourne
*/
void		_sub(t_list *champ, char *mem, int *arg)
{
  t_list        *champ;
  int           i;
  int           ret;
  int           res;

  i = 0;
  res = 0;
  res = _sub2(arg, i, ret, res);
  arg[3] = res;
  champ->carry = !champ->carry;
}

/*
** brief: la description
** @root: our list
** @mem: our memory
** @arg: our arguments
** @nb_champ: number of our champion
** return: -1 an error, 0 doesn't error
*/
int		_and(t_list *champ, char *mem, int *arg)
{
  int		nb;
  int		i;
  t_list	*champ;

  champ->carry = !champ->carry;
  i = -1;
  nb = type_arg(arg[0], 5, 6, 0);
  if (nb % 100 == 1 && (nb % 10 == 1 || nb / 10 == 1))
    {
      if (arg[1] == T_REG)
	_and_two_reg(arg[1], arg[3], arg[2]);
      else if (arg[2] == T_REG)
	_and_two_reg(arg[2], arg[3], arg[1]);
    }
  else if (nb % 100 == 1 && nb % 10 != 1 && nb / 10 != 1)
    while (arg[3][++i])
      arg[3][i] = arg[1] & arg[2];
  else if (nb % 100 == 1 && nb % 10 == 1 && nb / 10 == 1)
    while (arg[1][++i] && arg[2][i] && arg[3][i])
      arg[3][i] = arg[1][i] & arg[2][i];
  else
    return (-1);
  return (0);
}

/*
** brief: and against two register
** @reg1: first register
** @reg2: second register
** @val: the valeur
*/
static void    	_and_two_reg(int *reg1, int *reg2, int val)
{
  int		i;

  i = -1;
  while (arg[1][++i] && arg[2][i] && arg[3][i])
    reg2[i] = val & reg1[i];
}
